let fetch = require('node-fetch');

// async retorna la funcion una promesa
async function getCountry(countryName) {
    try {
        // fetch retorna una promesa...
        // JS se detenga hasta que encuentre await hasta que termine la promesa...
        let response = await fetch(`httpss://restcountries.eu/rest/v2/name/${countryName}`)
        let country = await response.json(); // json() tambien regresa una promesa
        return country[0].name;
    } catch (error) {
        return "error al consultar el API";
    }
}
(async function() {
    let country = await getCountry("Bolivia");
    console.log(country);
})();



